
function cacheFunction(cb) {
    let cacheObj = {};
    function callbackfn(...args){
      let key = args.toString();
      if(cacheObj[key] === undefined){
          let result = cb(...args);
          cacheObj[key] = result;
          return result;
      }
      else{
          return cacheObj[key];
      }

      
}

     return callbackfn;

}

module.exports = cacheFunction;