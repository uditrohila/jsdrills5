function counterFactory(){
    let counter = 0;

    let increment = function(){
        counter++;
        return counter;
    }

    let decrement = function(){
        counter--;
        return counter;
    }

    return {
        increment:increment,
        decrement: decrement
    }
}

// console.log(counterFactory.increment());
// console.log(counterFactory.decrement());
// const result = counterFactory();
// console.log(result.increment());
// console.log(result.decrement());


module.exports = counterFactory;