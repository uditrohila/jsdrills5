function limitFunctionCallCount(cb, n) {
  
       function invokecb(a,b,c){
        if(n >= 1)
        {
            n--;
            return cb(a,b,c);
        }
        else
        {
            return null;
        }
    }
   
       return invokecb;
       
   }
   
//    function cb(){
//        return 'hello closures';
//    }
   
module.exports = limitFunctionCallCount;