const cacheFunction = require("../cacheFunction");


const add = (a,b) => a + b;

const cachedAdd = cacheFunction(add);

console.log(cachedAdd(1,2)); // add should be invoked
console.log(cachedAdd(1,2)); // add should not be invoked but the same result returned from the cache
console.log(cachedAdd(2,1)); // add should be invoked
console.log(cachedAdd(2,3)); // add should be invoked
console.log(cachedAdd(2,3)); // add should not be invoked but the same result returned from the cache
 

 
// const doMath = (a,b,c) => a + b - c;

// const cachedDoMath = cacheFunction(doMath);

// console.log(cachedDoMath(1,2,3)); // doMath should be invoked
// console.log(cachedDoMath(1,2,3)); // doMath should not be invoked but the same result returned from the cache
// console.log(cachedDoMath(1,3,2)); // doMath should be invoked
// console.log(cachedDoMath(2,3,4)); // doMath should be invoked
// console.log(cachedDoMath(2,3,4)); // doMath should not be invoked but the same result returned from the cache
 



// const func = cacheFunction(function (arg){return arg*arg});
// const ans = func(5);
// console.log(ans);


// const ans1 = func(5);
// console.log(ans1);

// const ans2 = func(10);
// console.log(ans2);

// function cb(arg){
//     return arg*2;
// }

// const func1 = cacheFunction(cb);
// const ans = func1(2);
// console.log(ans);

// const ans1 = func1(3);
// console.log(ans1);

// const ans3 = func1(4);
// console.log(ans3);
